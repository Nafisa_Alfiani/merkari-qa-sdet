Feature: Create booking 
   Creating new booking for given user data

Scenario: Succes create booking with valid params
    Given valid create booking URL
    When I send valid JSON params with JSON header
    Then booking should be created

Scenario: Failed create booking on internal server error
    Given valid create booking URL
    When I send valid JSON params without JSON header
    Then I should receive internal server error

Scenario: Failed create booking on bad request
    Given valid create booking URL
    When I send valid JSON params with XML header
    Then I should receive bad request error