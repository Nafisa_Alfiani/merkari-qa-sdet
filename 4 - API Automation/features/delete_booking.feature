Feature: Delete booking
   Delete booking data based on booking ID

Scenario: Success delete booking
    Given valid delete booking URL
    When I delete valid booking ID with authorization
    Then booking should be deleted

Scenario: Failed delete booking on forbidden error 
    Given valid delete booking URL
    When I delete valid booking ID without authorization
    Then I should receive forbidden error