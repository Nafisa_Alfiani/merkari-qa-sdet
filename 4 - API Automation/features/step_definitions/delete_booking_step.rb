require 'rest-client'

Given("valid delete booking URL") do
  @url = "https://restful-booker.herokuapp.com/booking/"
end

When("I delete valid booking ID with authorization") do
  # Create booking for deletion purpose.
  resp_body = RestClient.post @url, "{\"firstname\":\"Jim\",\"lastname\":\"Brown\",\"totalprice\":111,\"depositpaid\":true,\"bookingdates\":{\"checkin\":\"2018-01-01\",\"checkout\":\"2019-01-01\"},\"additionalneeds\":\"Breakfast\"}", { "Content-Type" => "application/json" }
  resp_data = JSON.parse(resp_body)
  booking_id = resp_data["bookingid"]

  # Delete newly created booking.
  @do_delete = -> { RestClient.delete @url.concat(booking_id.to_s), { "Content-Type" => "application/json", "Authorization" => "Basic YWRtaW46cGFzc3dvcmQxMjM=" }}
end

Then("booking should be deleted") do 
  response = @do_delete.call
  expect(response.code).to eq(201)
end

When("I delete valid booking ID without authorization") do
  # Create booking for deletion purpose.
  resp_body = RestClient.post @url, "{\"firstname\":\"Jim\",\"lastname\":\"Brown\",\"totalprice\":111,\"depositpaid\":true,\"bookingdates\":{\"checkin\":\"2018-01-01\",\"checkout\":\"2019-01-01\"},\"additionalneeds\":\"Breakfast\"}", { "Content-Type" => "application/json" }
  resp_data = JSON.parse(resp_body)
  booking_id = resp_data["bookingid"]

  # Delete newly created booking.
  @do_delete = -> { RestClient.delete @url.concat(booking_id.to_s), { "Content-Type" => "application/json" }}
end

When("I should receive forbidden error") do
  expect{ @do_delete.call }.to raise_error(RestClient::Forbidden)
end 