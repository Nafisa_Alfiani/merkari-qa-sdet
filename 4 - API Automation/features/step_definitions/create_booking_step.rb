require 'rest-client'

Given("valid create booking URL") do
  @url = "https://restful-booker.herokuapp.com/booking"
end

When("I send valid JSON params with JSON header") do 
  @do_post = -> { RestClient.post @url, "{\"firstname\":\"Jim\",\"lastname\":\"Brown\",\"totalprice\":111,\"depositpaid\":true,\"bookingdates\":{\"checkin\":\"2018-01-01\",\"checkout\":\"2019-01-01\"},\"additionalneeds\":\"Breakfast\"}", { "Content-Type" => "application/json" }}
end

Then("booking should be created") do 
  response = @do_post.call
  expect(response.code).to eq(200)
end

When("I send valid JSON params without JSON header") do 
  @do_post = -> { RestClient.post @url, "{\"firstname\":\"Jim\",\"lastname\":\"Brown\",\"totalprice\":111,\"depositpaid\":true,\"bookingdates\":{\"checkin\":\"2018-01-01\",\"checkout\":\"2019-01-01\"},\"additionalneeds\":\"Breakfast\"}" }
end

Then("I should receive internal server error") do
  expect{ @do_post.call }.to raise_error(RestClient::InternalServerError)
end

When("I send valid JSON params with XML header") do
  @do_post = -> { RestClient.post @url, "{\"firstname\":\"Jim\",\"lastname\":\"Brown\",\"totalprice\":111,\"depositpaid\":true,\"bookingdates\":{\"checkin\":\"2018-01-01\",\"checkout\":\"2019-01-01\"},\"additionalneeds\":\"Breakfast\"}", { "Content-Type" => "application/xml" }}
end

When("I should receive bad request error") do
  expect{ @do_post.call }.to raise_error(RestClient::BadRequest)
end 
