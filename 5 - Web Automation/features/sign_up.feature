Feature: Sign Up
   Sign up for new user

Scenario: Create Amazon account successfully
    Given sign up page
    Then I fill Your Name
    Then I fill Email
    Then I fill Password
    Then I fill Re-Enter Password
    Then I click Create your Amazon  account
    Then I have Amazon account

Scenario: Can not create Amazon account on empty email, password, and re-enter password
    Given sign up page 
    Then I fill Your Name
    Then I click Create your Amazon  account
    Then I can't create Amazon account

Scenario: Can not create Amazon account when form not is filled
    Given sign up page 
    Then I click Create your Amazon  account
    Then I can't create Amazon account
