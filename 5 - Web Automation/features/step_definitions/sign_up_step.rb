require 'selenium-webdriver'

# configure the driver to run in headless mode
options = Selenium::WebDriver::Chrome::Options.new
options.add_argument('--headless')
driver = Selenium::WebDriver.for :chrome, options: options

Given("sign up page") do
  driver.navigate.to "https://www.amazon.com/ap/register?_encoding=UTF8&openid.assoc_handle=usflex&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2Fgp%2Fyourstore%2Fhome%3Fie%3DUTF8%26ref_%3Dnav_newcust"
end

Then("I fill Your Name") do
  driver.find_element(:id, "ap_customer_name").send_keys("Budi Anwar")
end

Then("I fill Email") do
  driver.find_element(:id, "ap_email").send_keys("budi.anwar@gmail.com")
end

Then("I fill Password") do
  driver.find_element(:id, "ap_password").send_keys("passwordqwerty12345")
end

Then("I fill Re-Enter Password") do
  driver.find_element(:id, "ap_password_check").send_keys("passwordqwerty12345")
end

Then("I click Create your Amazon  account") do
  driver.find_element(:id, "continue").click
  sleep(3)
end

Then("I have Amazon account") do
  expect(driver.current_url.include?("https://www.amazon.com/ap/cvf/request")).to eq(true)
end

Then("I can't create Amazon account") do
  expect(driver.current_url.include?("https://www.amazon.com/ap/register")).to eq(true)
end