require 'selenium-webdriver'

# configure the driver to run in headless mode
options = Selenium::WebDriver::Chrome::Options.new
options.add_argument('--headless')
driver = Selenium::WebDriver.for :chrome, options: options

Given("sign in page") do
  driver.navigate.to "https://www.amazon.com/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2F%3Fref_%3Dnav_ya_signin&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=usflex&openid.mode=checkid_setup&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&"
end

Then("I fill incorrect phone number") do
  driver.find_element(:id, "ap_email").send_keys("+6281234567898765432123345567789")
end

Then("I click sign in button") do
  driver.find_element(:id, "continue").click
  sleep(3)
end

Then("I told to enter valid phone number") do
  expect(driver.current_url.include?("https://www.amazon.com/ap/signin")).to eq(true)
end

Then("I told to enter email or phone number") do
  expect(driver.current_url.include?("https://www.amazon.com/ap/signin")).to eq(true)
end