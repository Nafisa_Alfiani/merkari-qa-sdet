Feature: Sign In
   Sign in registered user

Scenario: Failed sign in with incorrect phone number
    Given sign in page
    Then I fill incorrect phone number
    Then I click sign in button
    Then I told to enter valid phone number
  

Scenario: Failed sign in on empty email or phone number
    Given sign in page
    Then I click sign in button
    Then I told to enter email or phone number